(function() {

    var simplegit = require('simple-git');
    var APP = angular.module("Kadepot",['ngRoute', 'ngAnimate','ngTouch','ngCookies']);
    var fs = require('fs');

    var repo = fs.readFileSync('app/repo.json','utf8');

    APP.controller("MainController",function($scope) {
        $scope.repository = JSON.parse(repo);
        console.log($scope.repository);
    });

})();
